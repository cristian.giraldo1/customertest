import React from 'react';
import backgroundImage from './TigoAsesor.png'; // Asegúrate de ajustar la ruta según la ubicación del componente

const App = () => {
  const styles: React.CSSProperties = {
    backgroundImage: `url(${backgroundImage})`,
    backgroundSize: '100% 100%', 
    backgroundRepeat: 'no-repeat', 
    backgroundPosition: 'center', 
    width: '100%',
    height: '100vh',
    position: 'fixed',
    left: 0,
    zIndex: -1
  };

  return (
    <div style={styles}>
      {/* Contenido de tu aplicación */}
    </div>
  );
};

export default App;