import React from "react";
import "./styles.css";

interface CustomerProps {
  data: any; 
}


const ClientInfo: React.FC<CustomerProps> = ({data}) => {
  console.log(data,"from customer");
  let info=data?.data;
  const validateData = (field:string) =>{
  return field!=="" && field!==null && field!==undefined ? true : false;
  }
  return (
    <div className="contenedor">
      <div className="card cliente-info">
        <h1>Información del Cliente</h1>
        <p>
          <span className="icono">👤</span> Nombre:
          <span className="info">{validateData(info?.contact?.name)?
          info?.contact?.name:"No Disponible"}</span>
        </p>
        <p>
          <span className="icono">📞</span> Teléfono:{" "}
          <span className="info">{validateData(info?.contact?.phone_number)?
          info?.contact?.phone_number:"+57 123 456 7890"}</span>
        </p>
        <p>
          <span className="icono">📄</span> Número de Contrato:{" "}
          <span className="info">ABC123456</span>
        </p>
        <p>
          <span className="icono">💳</span> Facturas Pendientes:{" "}
          <span className="info">2</span>
        </p>
      </div>

      <div className="alertas">
        <div className="alerta">
          <span>⚠️ Cliente reincidente</span>
        </div>
        <div className="alerta">
          <span>💼 Oferta de Upgrade</span>
        </div>
      </div>

      <div className="acordeon">
        <div className="tab">
          <input type="checkbox" id="chck1" />
          <label className="tab-label" htmlFor="chck1">
            Soporte Técnico
          </label>
          <div className="tab-content">
            <p>
              Estado del Modem: <span className="info">En línea</span>
            </p>
            <p>
              Última caída de servicio: <span className="info">12/05/2024</span>
            </p>
            <p>
              Reportes de fallos: <span className="info">3</span>
            </p>
            <button>Reiniciar Modem</button>
          </div>
        </div>

        <div className="tab">
          <input type="checkbox" id="chck2" />
          <label className="tab-label" htmlFor="chck2">
            Detalles de Facturación
          </label>
          <div className="tab-content">
            <p>
              Última factura: <span className="info">$120.000</span>
            </p>
            <p>
              Fecha de vencimiento: <span className="info">30/05/2024</span>
            </p>
            <p>
              Estado de pago: <span className="info">Pendiente</span>
            </p>
            <button>Ver Factura</button>
          </div>
        </div>

        <div className="tab">
          <input type="checkbox" id="chck3" />
          <label className="tab-label" htmlFor="chck3">
            Detalles de Consumos
          </label>
          <div className="tab-content">
            <p>
              Consumo de datos: <span className="info">50GB / 100GB</span>
            </p>
            <p>
              Consumo de minutos:{" "}
              <span className="info">300 min / 500 min</span>
            </p>
            <p>
              SMS enviados: <span className="info">50 / 100</span>
            </p>
            <button>Ver Detalles</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ClientInfo;
