import React from 'react';
import './styles.css'; // Importa tus estilos CSS aquí

interface ButtonListProps {
  buttons: { [key: string]: string };
}

const ButtonList: React.FC<ButtonListProps> = ({ buttons }) => {
  const handleButtonClick = (url: string) => {
    window.open(url, '_blank');
  };

  return (
    <div className="button-list">
      {Object.entries(buttons).map(([name, url]) => (
        <button
          key={name}
          className="button-item"
          onClick={() => handleButtonClick(url)}
        >
          {name}
        </button>
      ))}
    </div>
  );
};

const Recursos = () => {
  const buttonData = {
    tigoAgent: "https://micuenta.tigo.com.co/agent/login/{{emailAgent}}/{{nameAgent}}/{{business}}/{{value}}?redirect=/home&token={{token}}&agentPlatform=Zendesk",
    siebel: "http://unecrm/ecommunications_esn/start.swe?",
    crmportal_na: "http://crmportal.tigo.com.co",
    pcrf: "http://10.69.42.32:81/SubsATP/perfilNintendo/index.php",
    cbs: "http://10.69.42.32:81/SubsATP/gestion_cbs/cambiar_estado/index_Actualizar.php",
    punto_contacto: "http://puntosdecontacto/",
    ciquery: "http://10.69.47.105:8081/query.asp?IMSI=&MSISDN={{value}}&Consultar.x=28&Consultar.y=13",
    ciinfranet: "http://10.69.47.105:8081/infranetCBS.asp?text1=&text3={{value}}&submit1=Consultar",
    cieventos: "http://10.69.47.105:8081/historicalGetPrepaidEvents.asp",
    citraking: "http://10.69.47.105:8081/IMEITrack.asp?IMEI=&MSISDN={{value}}&Consultar.x=36&Consultar.y=10",
    ciagencia: "http://10.69.47.105:8081/Agencias_Cobranza.asp?CODIGO_AG=&MOVIL={{value}}&CUENTA=&DOCUMENTO=&TELEFONO=&Consultar.x=66&Consultar.y=6",
    crmportal: "http://contact.tigo.com.co/CRMPortal/auth/portal/default/Postventa",
    tipiemtelco: "http://forma2.emtelco.co/dashboard",
    formados: "http://forma2.emtelco.co/",
    esurvey: "https://esurvey.emtelco.co/home/forms/",
    mimundotigo: "https://amstigo.com.co/wp-login.php",
    wiktigo: "https://wiktigo.emtelco.co/forma2/",
    commerce: "http://puntosdecontacto/images/tigo/Menus/e-commerce/",
    cmiportal: "https://cmi.amstigo.com.co/login.php",
    smartquick: "https://www.smartquick.com.co/tigo_une/inicio.php",
    portalconvergente: "http://172.20.4.122/Convergente/login.php",
    drepochip: "https://biblioteca.emtelco.co/tigo/tigo_repochip/",
    ivr: "http://10.69.47.105:8081/lista_blanca_IVR.asp",
    idctoken: "http://172.20.4.122/",
    politicapaz: "http://puntosdecontacto/index.php/36-fyq-hogares/1485-solicitud-paz-y-salvo",
    smnet: "http://172.20.2.62:8080/smnet/",
    cupo: "https://tigo.lisimsw.com/#/ing/login",
    cmi: "http://172.20.2.178/cmi_pymes/web/main/loginWithPassword",
    rues: "https://www.rues.org.co/",
    evidentemaster: "https://www.datacredito.com.co/",
    tigoexpress: "https://express.tigo.com.co/ ",
    emanager: "https://emanager.emtelco.co/#/",
    crm: "http://contact.tigo.com.co/CRMPortal/auth/portal/default/Consultas",
    // Agrega el resto de los botones aquí...
  };

  return (
    <div className="App">
      <h1>Lista de Botones</h1>
      <ButtonList buttons={buttonData} />
    </div>
  );
};

export default Recursos;
