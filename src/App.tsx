import React, { useState,useEffect} from 'react';
import { Tabs, Tab, Box } from '@mui/material';
import Recursos from "./Recursos"; 
import Customer from "./Customer"; 

const App = () => {
  const sections = [
    { title: "Recursos Externos", content: <Recursos /> },
    { title: "Información de Cliente", content: <Customer /> },
    { title: "Mi Tigo Asesor", content: <Recursos /> }
  ];
  const [selectedTab, setSelectedTab] = useState(0);

  const handleTabChange = (event:any, newValue:any) => {
    setSelectedTab(newValue);
  };

  const getData=async()=>{
    window.parent.postMessage('chatwoot-dashboard-app:fetch-info', '*')
  }
  
   
  
    useEffect(() => {
      window.addEventListener("message", function (event: MessageEvent<any>) {
        if (isJSONValid(event.data)) {
          console.log(JSON.parse(event.data),"event.data")
        }
      });
    }, []);

    function isJSONValid(data: string): boolean {
      try {
        JSON.parse(data);
        return true;
      } catch (e) {
        return false;
      }
    }

  return (
    <div>
      <button onClick={()=>getData()}>Solicitar data</button>
    <Box sx={{ width: '100%' }}>
      <Tabs
        value={selectedTab}
        onChange={handleTabChange}
        indicatorColor="primary"
        textColor="primary"
        centered
      >
        {sections.map((section, index) => (
          <Tab key={index} label={section.title} />
        ))}
      </Tabs>
      {sections.map((section, index) => (
        <TabPanel key={index} value={selectedTab} index={index}>
          {section.content}
        </TabPanel>
      ))}
    </Box>
    </div>
  );
};

const TabPanel = (props:any) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          {children}
        </Box>
      )}
    </div>
  );
};

export default App;
